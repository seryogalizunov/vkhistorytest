//
//  AppDelegate.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/9/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "AppDelegate.h"
#import <VKSdk.h>
#import "HTLoginViewController.h"
#import "HTNewsViewController.h"
#import "SWRevealViewController.h"
#import "HTNavigationController.h"
#import "HTRearViewController.h"

@interface AppDelegate () <SWRevealViewControllerDelegate>

@property (nonatomic, strong) SWRevealViewController *viewController;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    UIWindow *window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    [self setWindow:window];
    
    HTRearViewController *frontViewController = [[[HTRearViewController alloc] init] autorelease];
    HTLoginViewController *rearViewController = [self assemblyViewControllerWithWithIdentifier:NSStringFromClass([HTLoginViewController class])];
    
    HTNavigationController *frontNavigationController = [[[HTNavigationController alloc] initWithRootViewController:frontViewController] autorelease];
    HTNavigationController *rearNavigationController = [[[HTNavigationController alloc] initWithRootViewController:rearViewController] autorelease];
    
    SWRevealViewController *mainRevealController = [[[SWRevealViewController alloc]
                                                    initWithRearViewController:frontNavigationController frontViewController:rearNavigationController] autorelease];
    
    mainRevealController.delegate = self;
    [self setViewController:mainRevealController];
    
    [[self window] setRootViewController:[self viewController]];
    [[self window] makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{

}

- (void)applicationDidEnterBackground:(UIApplication *)application
{

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{

}

- (void)applicationWillTerminate:(UIApplication *)application
{
    
}

- (__kindof UIViewController *)assemblyViewControllerWithWithIdentifier:(NSString *)identifier
{
    return [[self assemblyStoryboard] instantiateViewControllerWithIdentifier:identifier];
}

- (UIStoryboard *)assemblyStoryboard
{
    return [UIStoryboard storyboardWithName:@"Main" bundle:nil];
}

#pragma mark - 
#pragma mark - SWRevealViewControllerDelegate

- (NSString*)stringFromFrontViewPosition:(FrontViewPosition)position
{
    NSString *str = nil;
    if ( position == FrontViewPositionLeft ) str = @"FrontViewPositionLeft";
    if ( position == FrontViewPositionRight ) str = @"FrontViewPositionRight";
    if ( position == FrontViewPositionRightMost ) str = @"FrontViewPositionRightMost";
    if ( position == FrontViewPositionRightMostRemoved ) str = @"FrontViewPositionRightMostRemoved";
    return str;
}

#pragma mark -
#pragma mark - VK SDK

- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary<NSString *,id> *)options
{
    [VKSdk processOpenURL:url
          fromApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]];
    return YES;
}

@end
