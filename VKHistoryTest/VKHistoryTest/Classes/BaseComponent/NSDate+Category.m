//
//  NSDate+Category.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/11/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "NSDate+Category.h"

@implementation NSDate (Category)

- (NSString *)converToString
{
    NSString *dateString = [NSDateFormatter localizedStringFromDate:self
                                                          dateStyle:NSDateFormatterMediumStyle
                                                          timeStyle:NSDateFormatterShortStyle];
    return dateString;
}

@end
