//
//  HTNavigationController.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/22/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTNavigationController.h"

@implementation HTNavigationController

#pragma mark -
#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self navigationItem] setHidesBackButton:YES animated:NO];
//    [[self view] setBackgroundColor:[UIColor colorWithRed:36.f/255 green:44.f/255 blue:57.f/255 alpha:1.f]];
    
    [self performNavigationBarTitleColor];
}

- (void)performNavigationBarTitleColor
{
    [[self navigationBar] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [[self navigationBar] setTranslucent:NO];
    [[self navigationBar] setBarTintColor:[UIColor colorWithRed:55.f/255 green:73.f/255 blue:118.f/255 alpha:1.f]];
}

@end
