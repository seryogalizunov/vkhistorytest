//
//  HTFriendsTableViewCell.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/10/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HTFriendsTableViewCell : UITableViewCell

- (void)performCellWithUserName:(NSString *)firstName
                       lastName:(NSString *)lastName
                      avatarUrl:(NSURL *)avatarUrl
                         userId:(NSInteger)userId;

@end
