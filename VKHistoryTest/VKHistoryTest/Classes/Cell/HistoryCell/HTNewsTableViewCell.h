//
//  HTHistoryTableViewCell.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/11/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <UIKit/UIKit.h>

@class  HTUser;
@class HTNews;

typedef void (^SuccessBlock)(id responseObject);

@interface HTNewsTableViewCell : UITableViewCell

- (void)performCellWithNews:(HTNews *)news;

@end
