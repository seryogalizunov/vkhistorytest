//
//  HTHistoryTableViewCell.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/11/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTNewsTableViewCell.h"
#import <PINRemoteImage/PINImageView+PINRemoteImage.h>
#import "NSDate+Category.h"
#import "HTNews.h"
#import "HTLikes.h"
#import "HTRepost.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface HTNewsTableViewCell ()

@property (retain, nonatomic) IBOutlet UIImageView *photoUserImageView;
@property (retain, nonatomic) IBOutlet UILabel *nameLabel;
@property (retain, nonatomic) IBOutlet UILabel *timeAgoLabel;
@property (retain, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (retain, nonatomic) IBOutlet UIImageView *photoNewsImageView;
@property (retain, nonatomic) IBOutlet UIButton *likesButton;
@property (retain, nonatomic) IBOutlet UILabel *likesCountLabel;
@property (retain, nonatomic) IBOutlet UILabel *repostCountLabel;
@property (assign, nonatomic) BOOL isLikes;

@end

@implementation HTNewsTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [[[self photoUserImageView] layer] setMasksToBounds:YES];
    [[[self photoUserImageView] layer] setCornerRadius:25];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

#pragma mark - 
#pragma mark - PerformCellWithNews

- (void)performCellWithNews:(HTNews *)news
{
    NSString *userName = [news userName];
    NSString *postDate = [[news postDate] converToString];
    NSURL *userPhotoUrl = [NSURL URLWithString:[self getStringWithUrl:[news photoUserUrl]]];
    NSURL *postPhotoUrl = [NSURL URLWithString:[self getStringWithUrl:[news photoPostUrl]]];
    NSString *descriptionText = [news descriptionText];
    NSString *likesCount = [NSString stringWithFormat:@"%ld", (long)[[news likesModel] countLikes]];
    UIImage *imageUserIfUserLokes = [self getImageIfUserLikes:[[news likesModel] userIsLikes]];
    NSString *repostCount = [self getStringWithInteget:[[news repostModel] repostCount]];
    BOOL islikes = [[news likesModel] userIsLikes];
    
    [[self nameLabel] setText:[userName retain]];
    [[self timeAgoLabel] setText:[postDate retain]];
    [[self photoUserImageView] sd_setImageWithURL:[userPhotoUrl retain]];
    [[self descriptionLabel] setText:[descriptionText retain]];
    [[self likesCountLabel] setText:[likesCount retain]];
    [[self likesButton] setImage:[imageUserIfUserLokes retain] forState:UIControlStateNormal];
    [[self repostCountLabel] setText:[repostCount retain]];
    [self setIsLikes:islikes];
    
    [[self photoNewsImageView] sd_setImageWithURL:[postPhotoUrl retain]];
        
    [userName release];
    [postDate release];
    [userPhotoUrl release];
    [descriptionText release];
    [likesCount release];
    [imageUserIfUserLokes release];
    [repostCount release];
    [postPhotoUrl release];
}

#pragma mark - 
#pragma mark - Actions

- (IBAction)likesActionButton:(UIButton *)sender
{
    self.isLikes = !self.isLikes;
    [[self likesButton] setImage:[self getImageIfUserLikes:[self isLikes]] forState:UIControlStateNormal];
}

#pragma mark - 
#pragma mark - Private Methods

- (UIImage *)getImageIfUserLikes:(BOOL)isLikes
{
    if (isLikes == YES)
    {
        return [UIImage imageNamed:@"onImage"];
    }
    else
    {
        return [UIImage imageNamed:@"offImage"];
    }
}

- (NSString *)getStringWithUrl:(NSURL *)url
{
    return [NSString stringWithFormat:@"%@", url];
}

- (NSString *)getStringWithInteget:(NSInteger)integer
{
    return [NSString stringWithFormat:@"%ld", (long)integer];
}

#pragma mark - 
#pragma mark - Dealloc

- (void)dealloc
{
    [_photoUserImageView release];
    [_nameLabel release];
    [_timeAgoLabel release];
    [_descriptionLabel release];
    [_photoNewsImageView release];
    [_likesButton release];
    [_likesCountLabel release];
    [_repostCountLabel release];
    [super dealloc];
}

@end
