//
//  HTSearchVideoTableViewCell.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/22/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTSearchVideoTableViewCell.h"
#import "HTVideoModel.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSDate+Category.h"

@interface HTSearchVideoTableViewCell ()

@property (retain, nonatomic) IBOutlet UIImageView *videoImageImageView;
@property (retain, nonatomic) IBOutlet UILabel *titleLable;
@property (retain, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (retain, nonatomic) IBOutlet UILabel *dateLabel;

@end

@implementation HTSearchVideoTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)performCellWithVideoModel:(HTVideoModel *)videoModel
{    
    [[self videoImageImageView] sd_setImageWithURL:[NSURL URLWithString:[videoModel videoImage]]];
    [[self titleLable] setText:[videoModel title]];
    [[self descriptionLabel] setText:[videoModel descriptionVideo]];
    [[self dateLabel] setText:[videoModel createDateString]];
}

- (void)dealloc
{
    [_videoImageImageView release];
    [_titleLable release];
    [_descriptionLabel release];
    [_dateLabel release];
    [super dealloc];
}
@end
