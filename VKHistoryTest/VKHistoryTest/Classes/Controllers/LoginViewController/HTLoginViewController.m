//
//  HTLoginViewController.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/9/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTLoginViewController.h"
#import "HTNewsViewController.h"
#import <VKSdk.h>
#import "HTNetworkManager.h"
#import "SWRevealViewController.h"

@interface HTLoginViewController () <UIWebViewDelegate, VKSdkUIDelegate, VKSdkDelegate>

@end

@implementation HTLoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[VKSdk initializeWithAppId:@"5719450"] registerDelegate:self];
    [[VKSdk instance] setUiDelegate:self];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"accessToken"] == nil)
    {
        NSArray *scope = @[@"friends", @"wall", @"email", @"audio", @"photos", @"email", @"messages", @"market", @"docs", @"video"];
        [VKSdk authorize:[scope retain]];
        [scope release];
    }
    else
    {
        [self performSegueWithIdentifier:@"HTHistoryViewController" sender:nil];
    }
}

#pragma mark -
#pragma mark - VKSdkUIDelegate

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller
{
    [[[self navigationController] topViewController] presentViewController:controller animated:YES completion:nil];
}

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError
{
    
}

#pragma mark - 
#pragma mark - VKSdkDelegate

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result
{
    [[NSUserDefaults standardUserDefaults] setObject:[[result token] accessToken] forKey:@"accessToken"];
    [[NSUserDefaults standardUserDefaults] setObject:[[result token] userId] forKey:@"userId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self performSegueWithIdentifier:@"HTHistoryViewController" sender:nil];
}

- (void)vkSdkUserAuthorizationFailed
{
    
}

#pragma mark - 
#pragma mark - prepareForSegue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    HTNewsViewController *newsViewController = (HTNewsViewController *)[segue destinationViewController];
    [newsViewController setAccessToken:[[NSUserDefaults standardUserDefaults] objectForKey:@"accessToken"]];
    [newsViewController setUserId:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
}

@end
