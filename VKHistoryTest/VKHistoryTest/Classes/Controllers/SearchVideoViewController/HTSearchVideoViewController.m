//
//  HTSearchVideoViewController.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/22/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTSearchVideoViewController.h"
#import "HTDataManager.h"
#import "HTSearchVideoTableViewCell.h"
#import "HTVideoModel.h"
#import "HTSearchVideoModel.h"
#import "HTSQLiteManager.h"

@interface HTSearchVideoViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) HTSearchVideoModel *searchVideoModel;

@end

@implementation HTSearchVideoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self searchBar] setDelegate:self];
    [self performSetTableView];
}

#pragma mark -
#pragma mark - PerformSetTableView

- (void)performSetTableView
{
    [[self tableView] setDelegate:self];
    [[self tableView] setDataSource:self];
    [[self tableView] setEstimatedRowHeight:100.0];
    [[self tableView] setRowHeight:UITableViewAutomaticDimension];
    [[self tableView] registerNib:[UINib nibWithNibName:@"HTSearchVideoTableViewCell" bundle:nil] forCellReuseIdentifier:@"HTSearchVideoTableViewCell"];
}

#pragma mark -
#pragma mark - PerformGetNewsRequest

- (void)performGetVideoSearchRequestWithKey:(NSString *)key
{
    __block typeof(self) blockSelf = self;
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        [[HTDataManager sharedManager] getSearchVideoWithAccessToken:[blockSelf accessToken]
//                                                                 key:key
//                                                           onSuccess:^(id responseObject)
//                                                            {
//                                                                [blockSelf setSearchVideoModel:[[[HTSearchVideoModel alloc] initWithVideoArray:(NSArray<HTVideoModel *> *)responseObject] autorelease]];
//            
//                                                                dispatch_async(dispatch_get_main_queue(), ^{
//                                                                    [[blockSelf tableView] reloadData];
//                                                                });
//                                                            }
//                                                           onFailure:^(NSError *error)
//                                                            {
//            
//                                                            }];
//    });
    [[HTDataManager sharedManager] getSearchVideoWithAccessToken:[self accessToken]
                                                             key:[key retain]
                                                       onSuccess:^(id responseObject)
                                                        {
                                                            [blockSelf setSearchVideoModel:[[[HTSearchVideoModel alloc] initWithVideoArray:(NSArray<HTVideoModel *> *)responseObject] autorelease]];
                                                            [[blockSelf tableView] reloadData];
                                                        }
                                                       onFailure:^(NSError *error)
                                                        {
         
                                                        }];
    [key release];
}

#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self searchVideoModel] videoArray] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *newsIdentifier = @"HTSearchVideoTableViewCell";
    
    HTSearchVideoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:newsIdentifier];
    [cell performCellWithVideoModel:[[[self searchVideoModel] videoArray] objectAtIndex:[indexPath row]]];
    return cell;
}

#pragma mark -
#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
//    [self performGetVideoSearchRequestWithKey:[searchBar text]];
    [searchBar resignFirstResponder];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self performGetVideoSearchRequestWithKey:[searchBar text]];
}

#pragma mark - 
#pragma mark - scrollViewWillBeginDragging

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [[self searchBar] resignFirstResponder];
}

#pragma mark -
#pragma mark - Dealloc

- (void)dealloc
{
    [_tableView release];
    [_searchBar release];
    [_searchVideoModel release];
    [super dealloc];
}

@end
