//
//  QABaseDataBaseManager.h
//  QuizApp
//
//  Created by Lizunov on 10/20/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "HTBaseDataBaseProtocol.h"

@interface HTBaseDataBaseManager : NSObject <HTBaseDataBaseProtocol>

@property (nonatomic, strong) NSOperationQueue* saveQueue;

@end
