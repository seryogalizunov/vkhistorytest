//
//  QABaseDataBaseManager.m
//  QuizApp
//
//  Created by Lizunov on 10/20/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "HTBaseDataBaseManager.h"

#define APPLICATION_NAME @"VKHistoryTest"

@interface HTBaseDataBaseManager ()

@property (nonatomic, strong) NSManagedObjectContext *persistentStoreContext;
@property (nonatomic, strong) NSManagedObjectContext *mainContext;
@property (nonatomic, strong) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end


@implementation HTBaseDataBaseManager

#pragma mark -
#pragma mark - Lifecycle

- (instancetype)init
{
    self = [super init];
    if(self)
    {
        [self setupDBManager];
        [self setSaveQueue:[[NSOperationQueue alloc] init]];
        [[self saveQueue] setMaxConcurrentOperationCount:2];
    }
    return self;
}

- (void)setupDBManager
{
    [self setManagedObjectModel:[NSManagedObjectModel mergedModelFromBundles:nil]];
    [self setPersistentStoreCoordinator:[[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite", APPLICATION_NAME]];
    NSError *error = nil;
    
    NSPersistentStore* store = [[self persistentStoreCoordinator] addPersistentStoreWithType:NSSQLiteStoreType
                                                                               configuration:nil
                                                                                         URL:storeURL
                                                                                     options:nil
                                                                                       error:&error];
    NSAssert(store != nil, @"Failed to initialize the application's saved data");
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator)
    {
        return;
    }
    
    [self setPersistentStoreContext:[[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType]];
    [[self persistentStoreContext] setPersistentStoreCoordinator:coordinator];
    
    [self setMainContext:[[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType]];
    [[self mainContext] setParentContext:[self persistentStoreContext]];
    [self setSaveQueue:[[NSOperationQueue alloc] init]];
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - 
#pragma mark - QABaseDataBaseProtocol

- (void)deleteObject:(NSManagedObject *)object
             context:(NSManagedObjectContext *)context
{
    if(!object)
    {
        NSLog(@"attemp to delete nil object from context %@", context);
        return;
    }
    
    if(!context)
    {
        NSLog(@"attemp to delete object from nil context %@", object);
        return;
    }
    
    if([object managedObjectContext] != context)
    {
        NSLog(@"attemp to delete object from other context %@", object);
        return;
    }
    
    NSLog(@"Will delete object from context %@", object);
    [context performBlockAndWait:^{
        [context deleteObject:object];
    }];
}

- (NSManagedObject*)existingObjectWithObjectID:(NSManagedObjectID *)managedObjectID
                                       context:(NSManagedObjectContext *)moc
{
    if (!managedObjectID)
    {
        NSAssert3(NO, @"%@ - %@ : %@", NSStringFromSelector(_cmd), NSStringFromClass([self class]), @"objectID can't be nil");
    }
    
    __block NSError *error;
    
    NSManagedObject *managedObject = [moc existingObjectWithID:managedObjectID
                                                         error:&error];
    
    if (error)
    {
        return nil;
    }
    
    return managedObject;
}

#pragma mark -
#pragma mark Setters/Getters

- (NSManagedObjectContext *)getMainContext
{
    return [self mainContext];
}

- (NSManagedObjectContext *)getNewDBContext
{
    NSManagedObjectContext* newDBContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [newDBContext setParentContext:[self mainContext]];
    return newDBContext;
}

#pragma mark -
#pragma mark Saving

- (void)saveContextSync:(NSManagedObjectContext *)context
{
    [self saveContextSync:context
       obtainPermanentIds:YES];
}

- (void)saveContextAsync:(NSManagedObjectContext *)context
{
    [self saveContextAsync:context
        obtainPermanentIds:YES
           completionBlock:nil];
}

- (void)saveContextAsync:(NSManagedObjectContext *)context
         completionBlock:(void (^)(void))block
{
    [self saveContextAsync:context
        obtainPermanentIds:YES
           completionBlock:block];
}

- (void)saveContextSync:(NSManagedObjectContext *)context
     obtainPermanentIds:(BOOL)obtain
{
    [context performBlockAndWait: ^{
        __autoreleasing NSError* error;
        if(obtain)
        {
            [context obtainPermanentIDsForObjects:[[context insertedObjects] allObjects]
                                            error:&error];
            if(error)
                NSLog(@"Failed to obtain permanent id's");
        }
        
        [context save: &error];
        
        if(error)
            NSLog(@"Failed to save context");
        
        if([context parentContext])
        {
            
            [self saveContextSync:[context parentContext]
               obtainPermanentIds:NO];
            
        }
    }];
}

- (void)saveContextAsync:(NSManagedObjectContext *)context
      obtainPermanentIds:(BOOL)obtain
         completionBlock:(void (^)(void))block
{
    [context performBlock: ^{
        __autoreleasing NSError* error;
        if(obtain)
        {
            [context obtainPermanentIDsForObjects:[[context insertedObjects] allObjects]
                                            error:&error];
            if(error)
                NSLog(@"Failed to obtain permanent id's");
        }
        
        [context save: &error];
        
        if([context parentContext])
        {
            if(block)
                [self saveContextAsync:[context parentContext]
                    obtainPermanentIds:NO
                       completionBlock:block];
            else
                [self saveContextSync:[context parentContext]
                   obtainPermanentIds:NO];
        }
        else
        {
            if(block)
                block();
        }
    }];
}

#pragma mark -
#pragma mark Helpers

- (id)newObjectForEntityName:(NSString *)entityName
                     context:(NSManagedObjectContext *)context
{
    if (!entityName || entityName.length == 0)
    {
        NSLog(@"No entity name for context %@", context);
        
        return nil;
    }
    
    if (!context)
    {
        NSLog(@"No context for entity name %@", entityName);
        
        return nil;
    }
    

    id newObject;
    @try {
        newObject = [NSEntityDescription insertNewObjectForEntityForName:entityName
                                                  inManagedObjectContext:context];;
        
    }
    @catch (NSException *exception) {
        NSLog(@"Exeption:: %@", exception);
    }
    
    return newObject;
}

@end
