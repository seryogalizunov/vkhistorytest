//
//  QACoreDataManager.h
//  VKHistoryTest
//
//  Created by Lizunov on 10/19/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "HTBaseDataBaseManager.h"
#import "HTCoreDataProtocol.h"

@interface HTCoreDataManager : HTBaseDataBaseManager <HTCoreDataProtocol>

@end
