//
//  QACoreDataManager.m
//  VKHistoryTest
//
//  Created by Lizunov on 10/19/16.
//  Copyright © 2016 nixsolutions. All rights reserved.
//

#import "HTCoreDataManager.h"
#import "HTSearchVideoModel.h"
#import "HTVideoDatabaseModel.h"
#import "HTVideoModel.h"

@implementation HTCoreDataManager

#pragma mark -
#pragma mark - HTVideoSearchDatabaseModel

- (HTVideoDatabaseModel *)newVideoSearch
{
    HTVideoDatabaseModel *videoSearchModel = [self newObjectForEntityName:@"HTVideoDatabaseModel" context:[self getMainContext]];
    return videoSearchModel;
}

- (NSArray <HTVideoDatabaseModel *> *)getVideoSeachrWithKey:(NSString *)key
{
    NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:@"HTVideoDatabaseModel"];
    NSArray <HTVideoDatabaseModel *> *searchVideoArray = [[self getMainContext] executeFetchRequest:request
                                                                                              error:nil];
    return [self filterSearchVideoArray:searchVideoArray
                                withKey:key];
}

#pragma mark -
#pragma mark - Save History To CoreDate

- (void)saveVideoSearchModel:(HTSearchVideoModel *)videoSearchModel
               completeBlock:(CoreDataManagerCompleteBlock)completeBlock
{
    for (NSInteger index = 0; index < [[videoSearchModel videoArray] count]; index++)
    {
        HTVideoDatabaseModel *videoSearchDatabaseModel = [[self newVideoSearch] autorelease];
        [videoSearchDatabaseModel setTitle:[[[videoSearchModel videoArray] objectAtIndex:index] title]];
        [videoSearchDatabaseModel setVideoImage:[[[videoSearchModel videoArray] objectAtIndex:index] videoImage]];
        [videoSearchDatabaseModel setCreateDateString:[[[videoSearchModel videoArray] objectAtIndex:index] createDateString]];
        [videoSearchDatabaseModel setDescriptionVideo:[[[videoSearchModel videoArray] objectAtIndex:index] descriptionVideo]];
        [self saveContextAsync:[self getNewDBContext]];
    }
    completeBlock();
}

#pragma mark - 
#pragma mark - Filter SearchVideoArray

- (NSArray <HTVideoDatabaseModel *> *)filterSearchVideoArray:(NSArray <HTVideoDatabaseModel *> *)videoDatabaseModelArray
                                                     withKey:(NSString *)key
{
    NSString *resultKey = [key lowercaseString];
    NSMutableArray <HTVideoDatabaseModel *> *videoArray = [[[NSMutableArray alloc] init] autorelease];
    for (NSInteger index = 0; index < [videoDatabaseModelArray count]; index++)
    {
        NSString *titleString = [[[videoDatabaseModelArray objectAtIndex:index] title] lowercaseString];
        if ([titleString rangeOfString:resultKey].location != NSNotFound)//([titleString hasPrefix:resultKey])//
        {
            HTVideoDatabaseModel *videoModel = [videoDatabaseModelArray objectAtIndex:index];
            [videoArray addObject:[videoModel retain]];
            [videoModel release];
        }
    }
    return videoArray;
}

#pragma mark -
#pragma mark - Convert VideoDatabaseModel In VideoSearchNetworkModel

- (HTSearchVideoModel *)convertVideoDatabaseModelInVideoSearchNetworkModel:(NSArray <HTVideoDatabaseModel *> *)videoDatabaseModelArray
{
    NSMutableArray *videoArray = [[[NSMutableArray alloc] init] autorelease];
    for (NSInteger index = 0; index < [videoDatabaseModelArray count]; index++)
    {
        HTVideoModel *videoModel = [[[HTVideoModel alloc] initWithTitle:[[videoDatabaseModelArray objectAtIndex:index] title]
                                                             videoImage:[[videoDatabaseModelArray objectAtIndex:index] videoImage]
                                                       descriptionVideo:[[videoDatabaseModelArray objectAtIndex:index] descriptionVideo]
                                                       createDateString:[[videoDatabaseModelArray objectAtIndex:index] createDateString]] autorelease];
        [videoArray addObject:[videoModel retain]];
        [videoModel release];
    }
    HTSearchVideoModel *searchVideoModel = [[[HTSearchVideoModel alloc] initWithVideoArray:[videoArray retain]] autorelease];
    [videoArray release];
    return searchVideoModel;
}

@end
