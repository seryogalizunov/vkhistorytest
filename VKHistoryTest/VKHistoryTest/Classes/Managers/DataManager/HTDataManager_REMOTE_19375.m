//
//  HTDataManager.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/24/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTDataManager.h"
#import "HTNetworkManager.h"
#import "HTSQLiteManager.h"
#import "HTSearchVideoModel.h"
#import "HTVideoModel.h"
#import "HTCoreDataManager.h"
#import "HTVideoDatabaseModel.h"

@interface HTDataManager ()

@property (nonatomic, strong) HTNetworkManager *networkManager;
@property (nonatomic, strong) HTSQLiteManager *databaseManager;
@property (nonatomic, strong) HTCoreDataManager *coreDataManager;

@end

@implementation HTDataManager

#pragma mark - 
#pragma mark - SharedManager

static HTDataManager *sharedAwardManager = nil;

+ (instancetype)sharedManager
{
    if (sharedAwardManager == nil)
    {
        sharedAwardManager = [[super allocWithZone:NULL] init];
    }
    return sharedAwardManager;
}

+ (instancetype)allocWithZone:(NSZone *)zone
{
    return [[self sharedManager] retain];
}

#pragma mark - 
#pragma mark - InitWithManagers

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self setNetworkManager:[[[HTNetworkManager alloc] init] autorelease]];
        [self setDatabaseManager:[[[HTSQLiteManager alloc] init] autorelease]];
        [self setCoreDataManager:[[[HTCoreDataManager alloc] init] autorelease]];
    }
    return self;
}

#pragma mark -
#pragma mark - Get NewsFeed

- (void)getNewsFeedWithAccessToken:(NSString *)accessToken
                       itemsOffset:(NSString *)itemsOffset
                         onSuccess:(DataManagerSuccessBlock)success
                         onFailure:(void (^) (NSError *error)) failure
{
    [[self networkManager] getNewsFeedWithAccessToken:accessToken
                                          itemsOffset:itemsOffset
                                            onSuccess:success
                                            onFailure:failure];
}

#pragma mark -
#pragma mark - Get UserProfile

- (void)getUserProfilesWithAccessToken:(NSString *)accessToken
                                userId:(NSString *)userId
                             onSuccess:(DataManagerSuccessBlock)success
                             onFailure:(void (^) (NSError *error)) failure
{
    [[self networkManager] getUserProfilesWithAccessToken:accessToken
                                                   userId:userId
                                                onSuccess:success
                                                onFailure:failure];
}

#pragma mark -
#pragma mark - Get UserPhotos

- (void)getUserPhotosWithAccessToken:(NSString *)accessToken
                              userId:(NSString *)userId
                           onSuccess:(DataManagerSuccessBlock)success
                           onFailure:(void (^) (NSError *error)) failure
{
    [[self networkManager] getUserPhotosWithAccessToken:accessToken
                                                 userId:userId
                                              onSuccess:success
                                              onFailure:failure];
}

#pragma mark -
#pragma mark - Get SearchVideo

- (void)getSearchVideoWithAccessToken:(NSString *)accessToken
                                  key:(NSString *)key
                            onSuccess:(DataManagerSuccessBlock)success
                            onFailure:(void (^) (NSError *error)) failure
{
    [self searchVideoWithAccessToken:[accessToken retain]
                                 key:[key retain]
                        successBlock:^(id responseObject)
                        {
                            success(responseObject);
                        }
                        failureBlock:failure];

    [key release];
    [accessToken release];
}

- (void)searchVideoWithAccessToken:(NSString *)accessToken
                                   key:(NSString *)key
                          successBlock:(DataManagerSuccessBlock)success
                          failureBlock:(void (^) (NSError *error)) failure
{
    __block typeof(self) blockSelf = self;
//    NSArray<HTVideoModel *> *videoArray = [[[self databaseManager] getSearchVideoModelWithKey:key] videoArray];
    
    NSArray <HTVideoDatabaseModel *> *videoArray = [[self coreDataManager] getVideoSeachrWithKey:key];
    HTSearchVideoModel *searchVideoModel = [[self coreDataManager] convertVideoDatabaseModelInVideoSearchNetworkModel:videoArray];
    
    if ([[searchVideoModel videoArray] count] > 0)//([videoArray count] > 0)
    {
        success([searchVideoModel videoArray]);
    }
    else
    {
        [[self networkManager] getSearchVideoWithAccessToken:accessToken
                                                         key:key
                                                   onSuccess:^(id responseObject)
                                                    {
                                                        HTSearchVideoModel *searchModel = responseObject[NSStringFromClass([HTSearchVideoModel class])];
                                                        [[blockSelf coreDataManager] saveVideoSearchModel:searchModel
                                                                                             successBlock:^(id responseObject)
                                                                                                {
                                                                                                    success([searchModel videoArray]);
                                                                                                }];
                                                        
//                                                        [blockSelf saveInDatabaseVideoSearchModel:[searchModel retain]
//                                                                                     successBlock:^(id responseObject)
//                                                                                        {
//                                                                                            success([searchModel videoArray]);
//                                                                                        }];
                                                    }
                                                   onFailure:failure];
    }
}

- (void)saveInDatabaseVideoSearchModel:(HTSearchVideoModel *)searchModel
                          successBlock:(DataManagerSuccessBlock)success
{
    for (NSInteger index = 0; index < [[searchModel videoArray] count]; index++)
    {
//        [[self databaseManager] saveSearchVideoModel:[[searchModel videoArray] objectAtIndex:index]];
    }
    
    success(nil);
}

#pragma mark -
#pragma mark - Dealloc

- (void)dealloc
{
    [_networkManager release];
    [_databaseManager release];
    [_coreDataManager release];
    [super dealloc];
}

@end
