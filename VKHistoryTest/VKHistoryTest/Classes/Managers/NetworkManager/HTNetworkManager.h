//
//  HTNetworkManager.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/9/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^NetworkManagerSuccessBlock)(id responseObject);

@interface HTNetworkManager : NSObject

- (instancetype)init;

- (void)getNewsFeedWithAccessToken:(NSString *)accessToken
                       itemsOffset:(NSString *)itemsOffset
                         onSuccess:(NetworkManagerSuccessBlock)success
                         onFailure:(void (^) (NSError *error)) failure;

- (void)getUserProfilesWithAccessToken:(NSString *)accessToken
                                userId:(NSString *)userId
                             onSuccess:(NetworkManagerSuccessBlock)success
                             onFailure:(void (^) (NSError *error)) failure;

- (void)getUserPhotosWithAccessToken:(NSString *)accessToken
                              userId:(NSString *)userId
                           onSuccess:(NetworkManagerSuccessBlock)success
                           onFailure:(void (^) (NSError *error)) failure;

- (void)getSearchVideoWithAccessToken:(NSString *)accessToken
                                  key:(NSString *)key
                            onSuccess:(NetworkManagerSuccessBlock)success
                            onFailure:(void (^) (NSError *error)) failure;

@end
