//
//  HTSQLiteManager.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/23/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTSQLiteManagerProtocol.h"

@interface HTSQLiteManager : NSObject <HTSQLiteManagerProtocol>

@end
