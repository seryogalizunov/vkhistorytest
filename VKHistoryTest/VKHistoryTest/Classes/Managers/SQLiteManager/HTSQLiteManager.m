//
//  HTSQLiteManager.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/23/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTSQLiteManager.h"
#import "FMDatabase.h"
#import "HTVideoModel.h"
#import "HTSearchVideoModel.h"

@interface HTSQLiteManager()

@property (nonatomic, strong) FMDatabase * database;

@end

@implementation HTSQLiteManager

#pragma mark - 
#pragma mark - Init

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        BOOL success;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *appDBPath = [documentsDirectory stringByAppendingPathComponent:@"TestDB.db"];
        success = [fileManager fileExistsAtPath:appDBPath];
        if (!success)
        {
            NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"TestDB.db"];
            success = [fileManager copyItemAtPath:defaultDBPath
                                           toPath:appDBPath
                                            error:&error];
        }
        [self setDatabase:[FMDatabase databaseWithPath:appDBPath]];
    }
    return self;
}

//CREATE TABLE videoModel(title primary key, videoImage text, descriptionVideo text, createDateText text);

#pragma mark - 
#pragma mark - Save SearchVideoModel

- (void)saveSearchVideoModel:(HTVideoModel *)searchVideoModel
{
    [[self database] open];
    NSString *insertQuery = @"INSERT INTO SearchVideo (title, videoImage, descriptionVideo, createDateText) VALUES (?, ?, ?, ?)";
    [[self database] executeUpdate:[insertQuery retain], [searchVideoModel title], [searchVideoModel videoImage], [searchVideoModel descriptionVideo], [searchVideoModel  createDateString], nil];
    [insertQuery release];
    [[self database] close];
}

#pragma mark -
#pragma mark - Get SearchVideoModel

- (HTSearchVideoModel *)getSearchVideoModelWithKey:(NSString *)key
{
    [[self database] open];
    NSString *resultKey = [key lowercaseString];
    HTSearchVideoModel *searchVideoModel = nil;
    NSMutableArray *videoArray = [[[NSMutableArray alloc] init] autorelease];
    FMResultSet *resultSet = [[self database] executeQuery:@"SELECT * FROM SearchVideo;"];
    NSInteger index = 0;
    while ([resultSet next])
    {
        NSString *titleString = [[resultSet stringForColumn:@"title"] lowercaseString];
        if ([titleString rangeOfString:resultKey].location != NSNotFound) //([titleString hasPrefix:resultKey])
        {
            HTVideoModel *videoModel = [[[HTVideoModel alloc] initWithTitle:[resultSet stringForColumn:@"title"]
                                                                 videoImage:[resultSet stringForColumn:@"videoImage"]
                                                           descriptionVideo:[resultSet stringForColumn:@"descriptionVideo"]
                                                           createDateString:[resultSet stringForColumn:@"createDateText"]] autorelease];
            [videoArray addObject:[videoModel retain]];
            [videoModel release];
            index++;
        }
    }
    
    if ([videoArray count] > 0)
    {
        for (NSInteger arrayIndex = 0; arrayIndex < ([videoArray count] - 1); arrayIndex++)
        {
            HTVideoModel *videoModel = [videoArray objectAtIndex:arrayIndex];
            HTVideoModel *tempVideoModel = [videoArray objectAtIndex:arrayIndex + 1];
            if ([[videoModel title] rangeOfString:[tempVideoModel title]].location != NSNotFound)
            {
                [videoArray removeObjectAtIndex:arrayIndex];
            }
        }
    }
    
    searchVideoModel =  [[[HTSearchVideoModel alloc] initWithVideoArray:[videoArray retain]] autorelease];
    [videoArray release];
    
    [[self database] close];
    return searchVideoModel;
}

@end
