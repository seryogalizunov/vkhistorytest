//
//  HTVideoDatabaseModel+CoreDataProperties.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/30/16.
//  Copyright © 2016 NIX. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "HTVideoDatabaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HTVideoDatabaseModel (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSString *videoImage;
@property (nullable, nonatomic, retain) NSString *descriptionVideo;
@property (nullable, nonatomic, retain) NSString *createDateString;

@end

NS_ASSUME_NONNULL_END
