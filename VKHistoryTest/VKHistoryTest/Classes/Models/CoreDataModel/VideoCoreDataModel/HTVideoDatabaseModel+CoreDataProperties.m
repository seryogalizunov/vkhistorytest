//
//  HTVideoDatabaseModel+CoreDataProperties.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/30/16.
//  Copyright © 2016 NIX. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "HTVideoDatabaseModel+CoreDataProperties.h"

@implementation HTVideoDatabaseModel (CoreDataProperties)

@dynamic title;
@dynamic videoImage;
@dynamic descriptionVideo;
@dynamic createDateString;

@end
