//
//  HTVideoDatabaseModel.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/30/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface HTVideoDatabaseModel : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "HTVideoDatabaseModel+CoreDataProperties.h"
