//
//  HTFriendInfo.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/10/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTFriendInfo : NSObject

@property (nonatomic, copy, readonly) NSString *firstName;
@property (nonatomic, copy, readonly) NSString *lastName;
@property (nonatomic, strong, readonly) NSURL *photoUrl;
@property (nonatomic, assign, readonly) NSInteger userId;

- (instancetype)initWithFirstName:(NSString *)firstName
                         lastName:(NSString *)lastName
                         photoUrl:(NSURL *)phoroUrl
                           userId:(NSInteger)userId;

@end
