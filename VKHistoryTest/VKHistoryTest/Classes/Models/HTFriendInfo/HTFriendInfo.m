//
//  HTFriendInfo.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/10/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTFriendInfo.h"

@interface HTFriendInfo ()

@property (nonatomic, copy, readwrite) NSString *firstName;
@property (nonatomic, copy, readwrite) NSString *lastName;
@property (nonatomic, strong, readwrite) NSURL *photoUrl;
@property (nonatomic, assign, readwrite) NSInteger userId;

@end

@implementation HTFriendInfo

- (instancetype)initWithFirstName:(NSString *)firstName
                         lastName:(NSString *)lastName
                         photoUrl:(NSURL *)phoroUrl
                           userId:(NSInteger)userId
{
    self = [super init];
    if (self)
    {
        [self setFirstName:firstName];
        [self setLastName:lastName];
        [self setPhotoUrl:phoroUrl];
        [self setUserId:userId];
    }
    return self;
}

- (void)dealloc
{
    [_firstName release];
    [_lastName release];
    [_photoUrl release];
    [super dealloc];
}

@end
