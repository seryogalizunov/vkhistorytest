//
//  HTUser.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/10/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HTNews;

@interface HTUser : NSObject

@property (nonatomic, strong, readonly) NSString *itemsOffset;
@property (nonatomic, copy, readonly) NSArray <HTNews *> *newsFeed;

- (instancetype)initWithNews:(NSArray<HTNews *> *)newsFeed
                 itemsOffset:(NSString *)itemsOffset;

@end
