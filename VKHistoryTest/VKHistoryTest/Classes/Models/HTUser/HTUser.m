//
//  HTUser.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/10/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTUser.h"
#import "HTNews.h"
#import "HTFriendInfo.h"

@interface HTUser ()

@property (nonatomic, strong, readwrite) NSString *itemsOffset;
@property (nonatomic, copy, readwrite) NSArray <HTNews *> *newsFeed;

@end

@implementation HTUser

- (instancetype)initWithNews:(NSArray<HTNews *> *)newsFeed
                   itemsOffset:(NSString *)itemsOffset
{
    self = [super init];
    if (self)
    {
        [self setNewsFeed:newsFeed];
        [self setItemsOffset:itemsOffset];
    }
    return self;
}

- (void)dealloc
{
    [_newsFeed release];
    [_itemsOffset release];
    [super dealloc];
}

@end
