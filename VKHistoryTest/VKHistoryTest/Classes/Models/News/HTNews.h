//
//  HTNews.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/11/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HTLikes;
@class HTRepost;

@interface HTNews : NSObject

@property (nonatomic, copy, readonly) NSString *userName;
@property (nonatomic, copy, readonly) NSString *descriptionText;
@property (nonatomic, strong, readonly) NSDate *postDate;
@property (nonatomic, strong, readonly) HTLikes *likesModel;
@property (nonatomic, strong, readonly) HTRepost *repostModel;
@property (nonatomic, strong, readonly) NSURL *photoUserUrl;
@property (nonatomic, strong, readonly) NSURL *photoPostUrl;
@property (nonatomic, strong, readonly) NSString *itemsOffset;

- (instancetype)initWithUserName:(NSString *)userName
                        postDate:(NSDate *)date
                      likesModel:(HTLikes *)likesModel
                     repostModel:(HTRepost *)repostModel
                    photoUserUrl:(NSURL *)photoUserUrl
                     photoPostUr:(NSURL *)photoPostUrl
                 descriptionText:(NSString *)text;

@end
