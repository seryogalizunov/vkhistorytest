//
//  HTNews.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/11/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTNews.h"
#import "HTLikes.h"
#import "HTRepost.h"
#import <PINRemoteImage/PINImageView+PINRemoteImage.h>


@interface HTNews ()

@property (nonatomic, copy, readwrite) NSString *userName;
@property (nonatomic, copy, readwrite) NSString *descriptionText;
@property (nonatomic, strong, readwrite) HTLikes *likesModel;
@property (nonatomic, strong, readwrite) HTRepost *repostModel;
@property (nonatomic, strong, readwrite) NSDate *postDate;
@property (nonatomic, strong, readwrite) NSURL *photoUserUrl;
@property (nonatomic, strong, readwrite) NSURL *photoPostUrl;

@end

@implementation HTNews

- (instancetype)initWithUserName:(NSString *)userName
                        postDate:(NSDate *)date
                      likesModel:(HTLikes *)likesModel
                     repostModel:(HTRepost *)repostModel
                    photoUserUrl:(NSURL *)photoUserUrl
                     photoPostUr:(NSURL *)photoPostUrl
                 descriptionText:(NSString *)text
{
    self = [super init];
    if (self)
    {
        [self setUserName:[userName retain]];
        [self setPostDate:[date retain]];
        [self setLikesModel:[likesModel retain]];
        [self setRepostModel:[repostModel retain]];
        [self setPhotoUserUrl:[photoUserUrl retain]];
        
        [self setPhotoPostUrl:[photoPostUrl retain]];
        [self setDescriptionText:[text retain]];
        
        [userName release];
        [date release];
        [likesModel release];
        [repostModel release];
        [photoUserUrl release];
        [photoPostUrl release];
        [text release];
        
        
//        NSData *data = UIImageJPEGRepresentation(image, 0.2);
//        [data writeToFile:fileName
//               atomically:YES];
//        [[self photoNewsImageView] pin_setImageFromURL:[NSURL URLWithString:[self getStringWithUrl:[news photoPostUrl]]]];
    }
    return self;
}

- (void)dealloc
{
    [_userName release];
    [_photoUserUrl release];
    [_postDate release];
    [_photoPostUrl release];
    [_likesModel release];
    [_repostModel release];
    [_descriptionText release];
    [super dealloc];
}

@end
