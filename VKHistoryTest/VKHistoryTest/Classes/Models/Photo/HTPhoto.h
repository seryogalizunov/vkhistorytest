//
//  HTPhoto.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/15/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTPhoto : NSObject

@property (nonatomic, strong, readonly) NSURL *photoUrl;

- (instancetype)initWithPhotoUrl:(NSURL *)photoUrl;

@end
