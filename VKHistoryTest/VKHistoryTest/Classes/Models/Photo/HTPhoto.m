//
//  HTPhoto.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/15/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTPhoto.h"

@interface HTPhoto ()

@property (nonatomic, strong, readwrite) NSURL *photoUrl;

@end

@implementation HTPhoto

- (instancetype)initWithPhotoUrl:(NSURL *)photoUrl
{
    self = [super init];
    if (self)
    {
        [self setPhotoUrl:photoUrl];
    }
    return self;
}

@end
