//
//  HTPhotoAlbum.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/15/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HTPhoto;

@interface HTPhotoAlbum : NSObject

@property (nonatomic, copy, readonly) NSArray <HTPhoto *> *photosArray;

- (instancetype)initWithPhotosArray:(NSArray <HTPhoto *> *)photosArray;

@end
