//
//  HTPhotoAlbum.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/15/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTPhotoAlbum.h"
#import "HTPhoto.h"

@interface HTPhotoAlbum ()

@property (nonatomic, copy, readwrite) NSArray <HTPhoto *> *photosArray;

@end

@implementation HTPhotoAlbum

 - (instancetype)initWithPhotosArray:(NSArray<HTPhoto *> *)photosArray
{
    self = [super init];
    if (self)
    {
        [self setPhotosArray:photosArray];
    }
    return self;
}

@end
