//
//  HTProfile.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/15/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTProfile : NSObject

@property (nonatomic, copy, readonly) NSString *firstName;
@property (nonatomic, copy, readonly) NSString *lastName;
@property (nonatomic, copy, readonly) NSURL *photoUrl;
@property (nonatomic, copy, readonly) NSString *dateOfBirth;
@property (nonatomic, copy, readonly) NSString *university;
@property (nonatomic, copy, readonly) NSString *school;

- (instancetype)initWithFirstName:(NSString *)firstName
                         lastName:(NSString *)lastName
                         photoUrl:(NSURL *)photoUrl
                      dateOfBirth:(NSString *)dateOfBirth
                       university:(NSString *)university
                           school:(NSString *)school;

@end
