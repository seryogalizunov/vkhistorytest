//
//  HTProfile.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/15/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTProfile.h"

@interface HTProfile ()

@property (nonatomic, copy, readwrite) NSString *firstName;
@property (nonatomic, copy, readwrite) NSString *lastName;
@property (nonatomic, copy, readwrite) NSURL *photoUrl;
@property (nonatomic, copy, readwrite) NSString *dateOfBirth;
@property (nonatomic, copy, readwrite) NSString *university;
@property (nonatomic, copy, readwrite) NSString *school;

@end

@implementation HTProfile

- (instancetype)initWithFirstName:(NSString *)firstName
                         lastName:(NSString *)lastName
                         photoUrl:(NSURL *)photoUrl
                      dateOfBirth:(NSString *)dateOfBirth
                       university:(NSString *)university
                           school:(NSString *)school
{
    self = [super init];
    if (self)
    {
        [self setFirstName:firstName];
        [self setLastName:lastName];
        [self setPhotoUrl:photoUrl];
        [self setDateOfBirth:dateOfBirth];
        [self setUniversity:university];
        [self setSchool:school];
    }
    return self;
}

- (void)dealloc
{
    [_firstName release];
    [_lastName release];
    [_photoUrl release];
    [_dateOfBirth release];
    [_university release];
    [_school release];
    [super dealloc];
}

@end
