//
//  HTRepost.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/14/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTRepost : NSObject

@property (nonatomic, assign, readonly) NSInteger repostCount;

- (instancetype)initWithRepostCount:(NSInteger)repostCount;

@end
