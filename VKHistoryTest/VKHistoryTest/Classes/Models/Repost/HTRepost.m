//
//  HTRepost.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/14/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTRepost.h"

@interface HTRepost ()

@property (nonatomic, assign, readwrite) NSInteger repostCount;

@end

@implementation HTRepost

- (instancetype)initWithRepostCount:(NSInteger)repostCount
{
    self = [super init];
    if (self)
    {
        [self setRepostCount:repostCount];
    }
    return self;
}

@end
