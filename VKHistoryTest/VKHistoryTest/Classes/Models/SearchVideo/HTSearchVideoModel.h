//
//  HTSerchVideoModel.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/22/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HTVideoModel;

@interface HTSearchVideoModel : NSObject

@property (nonatomic, copy, readonly) NSArray <HTVideoModel *> *videoArray;

-(instancetype)initWithVideoArray:(NSArray <HTVideoModel *> *)videoArray;

@end
