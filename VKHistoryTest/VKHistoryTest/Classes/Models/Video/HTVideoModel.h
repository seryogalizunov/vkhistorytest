//
//  HTVideoModel.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/22/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTVideoModel : NSObject //<NSCoding>

@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, copy, readonly) NSString *videoImage;
@property (nonatomic, copy, readonly) NSString *descriptionVideo;
@property (nonatomic, copy, readonly) NSString *createDateString;

- (instancetype)initWithTitle:(NSString *)title
                   videoImage:(NSString *)videoImage
             descriptionVideo:(NSString *)descriptionVideo
             createDateString:(NSString *)createDate;

@end
