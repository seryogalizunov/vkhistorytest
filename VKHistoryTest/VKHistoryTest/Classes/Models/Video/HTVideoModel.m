//
//  HTVideoModel.m
//  VKHistoryTest
//
//  Created by Lizunov on 11/22/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "HTVideoModel.h"

@interface HTVideoModel ()

@property (nonatomic, copy, readwrite) NSString *title;
@property (nonatomic, copy, readwrite) NSString *videoImage;
@property (nonatomic, copy, readwrite) NSString *descriptionVideo;
@property (nonatomic, copy, readwrite) NSString *createDateString;

@end

@implementation HTVideoModel

- (instancetype)initWithTitle:(NSString *)title
                   videoImage:(NSString *)videoImage
             descriptionVideo:(NSString *)descriptionVideo
             createDateString:(NSString *)createDate
{
    self = [super init];
    if (self)
    {
        [self setTitle:[title retain]];
        [self setVideoImage:[videoImage retain]];
        [self setDescriptionVideo:[descriptionVideo retain]];
        [self setCreateDateString:[createDate retain]];
        
        [title release];
        [videoImage release];
        [descriptionVideo release];
        [createDate release];
    }
    return self;
}

//- (void)encodeWithCoder:(NSCoder *)aCoder
//{
//    [aCoder encodeObject:[self title] forKey:@"title"];
//    [aCoder encodeObject:[self videoImage] forKey:@"videoImage"];
//    [aCoder encodeObject:[self descriptionVideo] forKey:@"descriptionVideo"];
//    [aCoder encodeObject:[self createDateString] forKey:@"createDateString"];
//}
//
//-(id)initWithCoder:(NSCoder *)aDecoder
//{
//    if(self = [super init])
//    {
//        self.title = [aDecoder decodeObjectForKey:@"title"];
//        self.videoImage = [aDecoder decodeObjectForKey:@"videoImage"];
//        self.descriptionVideo = [aDecoder decodeObjectForKey:@"descriptionVideo"];
//        self.createDateString = [aDecoder decodeObjectForKey:@"createDateString"];
//    }
//    return self;
//}

- (void)dealloc
{
    [_title release];
    [_videoImage release];
    [_descriptionVideo release];
    [_createDateString release];
    [super dealloc];
}

@end
