//
//  HTBaseDataBaseProtocol.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/30/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HTBaseDataBaseProtocol <NSObject>

- (instancetype)init;

- (NSManagedObjectContext *)getMainContext;

- (NSManagedObjectContext *)getNewDBContext;

- (void)saveContextSync:(NSManagedObjectContext *)context;

- (void)saveContextAsync:(NSManagedObjectContext *)context;

- (void)saveContextAsync:(NSManagedObjectContext *)context
         completionBlock:(void (^)(void))block;

- (void)deleteObject:(NSManagedObject *)object
             context:(NSManagedObjectContext *)context;

- (id)existingObjectWithObjectID:(NSManagedObjectID *)managedObjectID
                         context:(NSManagedObjectContext *)moc;

- (id)newObjectForEntityName:(NSString *)entityName
                     context:(NSManagedObjectContext *)context;

@end
