//
//  HTCoreDataProtocol.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/30/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HTVideoDatabaseModel;
@class HTSearchVideoModel;

typedef void (^CoreDataManagerCompleteBlock)(void);

@protocol HTCoreDataProtocol <NSObject>

#pragma mark -
#pragma mark - HTVideoSearchDatabaseModel

- (HTVideoDatabaseModel *)newVideoSearch;
- (NSArray <HTVideoDatabaseModel *> *)getVideoSeachrWithKey:(NSString *)key;

#pragma mark -
#pragma mark - Save History To CoreDate

- (void)saveVideoSearchModel:(HTSearchVideoModel *)videoSearchModel
                completeBlock:(CoreDataManagerCompleteBlock)completeBlock;

#pragma mark -
#pragma mark - convertVideoDatabaseModelInVideoSearchNetworkModel

- (HTSearchVideoModel *)convertVideoDatabaseModelInVideoSearchNetworkModel:(NSArray <HTVideoDatabaseModel *> *)videoDatabaseModelArray;

@end
