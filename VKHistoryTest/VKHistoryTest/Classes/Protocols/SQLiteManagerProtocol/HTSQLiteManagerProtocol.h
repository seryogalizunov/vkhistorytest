//
//  HTSQLiteManagerProtocol.h
//  VKHistoryTest
//
//  Created by Lizunov on 11/24/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HTVideoModel;
@class HTSearchVideoModel;

typedef void (^DataBaseManagerSuccessBlock)(id responseObject);

@protocol HTSQLiteManagerProtocol <NSObject>

- (instancetype)init;

- (void)saveSearchVideoModel:(HTVideoModel *)searchVideoModel;

//- (void)saveSearchVideoWithSeacrhVideoArray:(NSArray *)searchVideoArray
//                                     andKey:(NSString *)key;

- (HTSearchVideoModel *)getSearchVideoModelWithKey:(NSString *)key;

@end
